import com.wizard.CharacterCreation;
import com.wizard.Wizard;
import com.wizard.WizardShop;

import java.util.Scanner;

public class WizardWars {
    public static void main(String[] args) {

        /**
         * Task 3:
         * Start character creation.
         * We want to get name and surname and then we will set it in wizard field surname and name.
         * But do not forget to validate first the name and surname then save it surname and name.
         */

        CharacterCreation characterCreation = new CharacterCreation();
        System.out.println("Welcome to Wizard Wars. Lets create our character");
        Wizard createdWizard = new Wizard();
        Scanner scanner = new Scanner(System.in);

        while (createdWizard.getName() == null || createdWizard.getName().isEmpty()) {
            System.out.println("Please enter your characters' first name");
            String incomingName = scanner.nextLine();
            if (characterCreation.validateName(incomingName)) {
                createdWizard.setName(incomingName);
            }
        }

        while (createdWizard.getSurname() == null || createdWizard.getName().isEmpty()) {
            System.out.println("Please enter your characters' surname");
            String incomingSurname = scanner.nextLine();
            if (characterCreation.validateSurname(incomingSurname)) {
                createdWizard.setSurname(incomingSurname);
            }
        }

        //We got the surname and the name inside the createdWizard object
        System.out.println("Welcome " + createdWizard.getName() + " " + createdWizard.getSurname());

        WizardShop wizardShop=new WizardShop();
        System.out.println("Welcome to Wizard Shop. Can i take your order? Write spell Name to buy.");
        wizardShop.printAllSpells();


    }

}

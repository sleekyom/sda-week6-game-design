package com.wizard;


import java.util.Random;

public class SpellUtilities {
    Random random = new Random();

    /**
     * Spell Word:          Lacarnum Inflamarae
     * Price:               25
     * Damage:              10-25
     * Success Rate         70%
     */

    public int castFlame() {
        //creating 0 inclusive to bound (exclusive)
        int castRate = random.nextInt(100);
        if (castRate < 70) {
            System.out.println("Lacarnum Inflamarae casted");
            return getRandomNumberInRange(10, 25);
        }
        System.out.println("Lacarnum Inflamarae Casting failed.");
        return 0;
    }

    /**
     * Spell Word:          Lumos Solem
     * Price:               45
     * Damage:              17-35
     * Success Rate         60%
     *
     * @return
     */
    public int castLightening() {

    }
    /**
     * Spell Word:          Everte Statum
     * Price:               45
     * Damage:              23-29
     * Success Rate         65%
     *
     * @return
     */
    public int castWind() {

    }
    /**
     * Spell Word:          Arania Exumai
     * Price:               50
     * Damage:              30-45
     * Success Rate         50%
     *
     * @return
     */
    public int castFireball() {

    }
    /**
     * Spell Word:          Bombarda
     * Price:               60
     * Damage:              37-45
     * Success Rate         65%
     *
     * @return
     */
    public int castExplosion() {

    }
    /**
     * Spell Word:          Avada Kedavra
     * Price:               100
     * Damage:              100
     * Success Rate         10%
     *
     * @return
     */
    public int castDeath() {

    }
    /**
     * Spell Word:          Vulnera Sanentur
     * Price:               25
     * Healing:             10-20
     * Success Rate         70%
     *
     * @return healing amount
     */
    public int castHealing() {

    }
    private int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            System.out.println("max must be greater than min");
            return 0;
        }
        //for 10 25 information it will be nextInt(16) = means that it return numbers including
        //0 to 15
        int numberGenerated = random.nextInt((max - min) + 1);
        return numberGenerated + min;

    }


    private int LacarnumInflamarae() {
        int price = 25;

        int successRate = 70 %;

        System.out.println();



    }

    public static void main(String[] args) {
        int minDamage = 10;
        int maxDamage = 25;
        int range = maxDamage - minDamage + 1;
        for (int damage = 0; damage <= 25; damage++) {
            System.out.println((int) Math.random() + minDamage);
        }
    }


}

package com.wizard;

import java.util.HashMap;

public class WizardShop {


    private HashMap<String, Integer> spellMap = new HashMap<>();

    public WizardShop() {

        spellMap.put("Lacarnum Inflamarae", 25);
        spellMap.put("Lumos Solem", 45);
        spellMap.put("Everte Statum", 25);
        spellMap.put("Arania Exumai", 50);
        spellMap.put("Bombarda", 60);
        spellMap.put("Avada Kedavra", 100);
        spellMap.put("Vulnera Sanentur", 25);
    }

    public void printAllSpells() {
        System.out.println(spellMap);
    }

    public void buy(String requestedSpell, Wizard wizard) {
        if (wizard.getListOfSpellsWizardKnows().contains(requestedSpell)) {
            System.out.println("You already know this spell");
            return;
        }
        if (!spellMap.containsKey(requestedSpell)) {
            System.out.println("This spell doesn't exist in the shop");
            return;
        }
        if (wizard.getMoney() < spellMap.get(requestedSpell)) {
            System.out.println("You don't have enough money to buy this spell");
        }


        System.out.println("You bought " + requestedSpell);
        int costOfSpell = spellMap.get(requestedSpell);
        int remainingMoney = wizard.getMoney() - costOfSpell;
        wizard.setMoney(remainingMoney);
        wizard.getListOfSpellsWizardKnows().add(requestedSpell);
    }


}
